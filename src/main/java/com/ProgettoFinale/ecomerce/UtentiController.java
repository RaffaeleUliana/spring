package com.ProgettoFinale.ecomerce;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ProgettoFinale.model.Oggetto;
import com.ProgettoFinale.model.Utente;
import com.ProgettoFinale.services.UtenteDAO;


@RestController
@RequestMapping("/utente")
@CrossOrigin("http://localhost:4200")
public class UtentiController {

	@PostMapping("/registrazione")
	public boolean registraUtente(@RequestBody Utente U){
		
				
		UtenteDAO tempDao= new UtenteDAO();

		try {
			
			tempDao.insert(U);
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		
		
		
	
	}
	
		
		
	@PostMapping("/controllaUtente")
	public boolean checkUser(@RequestBody Utente U){
		
		UtenteDAO tempDao= new UtenteDAO();
		Utente temp = null;
		try {
			
			temp =tempDao.getUtente(U);
			if(temp !=null) {
				return true;
			}else {
				return false;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		
		
	
	}
	@PostMapping("/eliminaUtente")
	public boolean deleteUser(@RequestBody String username){
		
		UtenteDAO tempDao= new UtenteDAO();
		
		try {
			
			
			if(tempDao.deletebyUser(username)) {
				return true;
			}else {
				return false;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		
		
	
	}
	
	
	
}

